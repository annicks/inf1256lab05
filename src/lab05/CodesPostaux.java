/**
 * @author Johnny Tsheke @ UQAM
 * 2017-02-14
 */
package lab05;

/**
 * @author johnny
 *
 */
import java.util.*;
public class CodesPostaux {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		final String CP1 = "H3A 3Z1";
		final String CP2 = "H2A 1L1";
		final String CP3 = "H8T 3N1";
		final String CP4 = "H2A 5T1";
		final String CP5 = "H1B 8Q7";
        final String DELIMITER = "\n";//caractère de fin de lecture d'une entrée. par défaut c'est un espace
		System.out.format("Voici les codes postaux autorisés %n");
		System.out.format("%s%n%s%n%s%n%s%n%s%n", CP1,CP2,CP3,CP4,CP5); //retour à la ligne apres chaque code
        String pattern =CP1+"|"+CP2+"|"+CP3+"|"+CP4+"|"+CP5;
        boolean bonCode = false;
        Scanner clavier = new Scanner(System.in);
        clavier.useDelimiter(DELIMITER);//on spécifie le délimiteur d'une entrée
        
        while(!bonCode){
        	System.out.format("Entrez un code postal parmis les suivants svp %n");
        	System.out.format("%s%n%s%n%s%n%s%n%s%n", CP1,CP2,CP3,CP4,CP5);
        	if(clavier.hasNext(pattern)){
        		bonCode = true;
        	}else{
        		System.out.println("Code postal incorrect!");
        		clavier.next();//on deplace la tête de lecture et on jete la saisie  actuelle
        	}
        }
        String codePostal = clavier.next();
        System.out.format("Le code postal accepté est %s%n",codePostal);
		clavier.close();
	}

}
