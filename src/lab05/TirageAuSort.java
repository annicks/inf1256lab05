/**
 * @author Johnny Tsheke @ UQAM
 * 2017-02-14
 */
package lab05;
import java.lang.*;
import java.util.*;
/**
 * @author johnny
 *
 */
public class TirageAuSort {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		final int NOMBRE_MIN = 500;
		final int NOMBRE_MAX = 535;
		System.out.format("Tirage au sort d'un numéro entre %d et %d %n",NOMBRE_MIN,NOMBRE_MAX);
		int intervalle = NOMBRE_MAX - NOMBRE_MIN;
		double aleatoire = Math.random();
		double nombreAletoire = intervalle * aleatoire;
		//int nombreChoisit = (int)Math.rint(nombreAletoire);
		long nombreArrondi = Math.round(nombreAletoire);
		long nombreChoisi = NOMBRE_MIN + nombreArrondi;
		System.out.format("Le nombre tiré au sort est %d %n", nombreChoisi);
		
		

	}

}
